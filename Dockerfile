FROM alpine:3.10

# install docker client binary
ARG DOCKER_CLI_VERSION="19.03.7"
ENV DOWNLOAD_URL="https://download.docker.com/linux/static/stable/x86_64/docker-$DOCKER_CLI_VERSION.tgz"
RUN apk --update add curl \
    && mkdir -p /tmp/download \
    && curl -L $DOWNLOAD_URL | tar -xz -C /tmp/download \
    && mv /tmp/download/docker/docker /usr/local/bin/ \
    && rm -rf /tmp/download \
    && apk del curl \
    && rm -rf /var/cache/apk/*

# install jobber
ENV JOBBER_VERSION 1.4.0
ENV JOBBER_SHA256 37a96591e2c28494ef009d900a4c680c4fbd3c82bf4e6de3f70c6ad451e45867

RUN wget -O /tmp/jobber.apk "https://github.com/dshearer/jobber/releases/download/v${JOBBER_VERSION}/jobber-${JOBBER_VERSION}-r0.apk" && \
    echo "${JOBBER_SHA256} */tmp/jobber.apk" | sha256sum -c && \
# --no-scripts is needed b/c the post-install scripts don't work in Docker
    apk add --no-network --no-scripts --allow-untrusted /tmp/jobber.apk && \
    rm /tmp/jobber.apk

RUN mkdir -p /var/jobber/0

USER root
CMD ["/usr/libexec/jobberrunner", "-u", "/var/jobber/0/cmd.sock", "/etc/jobber/.jobber"]